<?php
declare(strict_types=1);

namespace RouteeBaseApi\Utils;

class UrlHandler
{
    public static function mergeUrl(string $base, string $action):string
    {
        return rtrim($base, '/').'/'.trim($action, '/');
    }
}
