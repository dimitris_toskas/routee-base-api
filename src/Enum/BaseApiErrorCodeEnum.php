<?php
declare(strict_types=1);

namespace RouteeBaseApi\Enum;

class BaseApiErrorCodeEnum
{
    const MISSING_ACTION        = 7000;
    const MISSING_REQUEST_FIELD = 7001;
    const MISSING_ENTITY        = 7002;

    public static function getMessage(int $errorCode, array $options = []): string
    {
        $message = '';
        switch ($errorCode) {
            case self::MISSING_ACTION:
                $message = 'Missing Action';
                break;
            case self::MISSING_ENTITY:
                $message = 'Missing Entity';
                break;
            case self::MISSING_REQUEST_FIELD:
                $fieldName = $options['fieldname'] ?? "";
                $message   = 'Required field "' . $fieldName . '" is missing';
                break;
            default:
                break;
        }
        return $message;
    }
}
