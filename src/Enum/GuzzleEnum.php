<?php
declare(strict_types=1);

namespace RouteeBaseApi\Enum;

class GuzzleEnum extends BaseApiEnum
{
    const QUERY       = 'query';
    const FORM_PARAMS = 'form_params';
    const JSON        = 'json';
}
