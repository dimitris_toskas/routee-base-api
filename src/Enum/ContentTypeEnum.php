<?php
declare(strict_types=1);

namespace RouteeBaseApi\Enum;

class ContentTypeEnum
{
    const APPLICATION_JSON = 'application/json';
}
