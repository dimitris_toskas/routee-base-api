<?php
declare(strict_types=1);

namespace RouteeBaseApi\Enum;

class HeadersEnum
{
    const ACCEPT        = 'Accept';
    const AUTHORIZATION = 'Authorization';
    const CONTENT_TYPE  = 'Content-Type';
}