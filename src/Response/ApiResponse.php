<?php
declare(strict_types=1);

namespace RouteeBaseApi\Response;

use Exception;
use Psr\Http\Message\ResponseInterface;
use RouteeBaseApi\Exception\MissingEntityException;

class ApiResponse extends BaseResponse
{
    /**
     * @var bool
     */
    protected $isSuccess = true;
    /**
     * @var array|Exception|string
     */
    protected $data = array();
    /**
     * @var string
     */
    protected $message = '';

    protected $entityClassToTransform = null;

    public function __construct($response)
    {
        parent::__construct($response);
        if ($response instanceof \Exception) {
            $this->isSuccess = false;
            $this->data = $response;
            $this->message = $response->getMessage();
        } else {
            /**
             * @var ResponseInterface $response
             */
            $this->data = $this->getContents();
            $this->isSuccess = true;
            $statusCode = $this->getStatusCode();
            if ($statusCode > 299) {
                $this->isSuccess = false;
            }
        }
    }

    public function toArray(): array
    {
        return array(
            'isSuccess' => $this->isSuccess,
            'data'      => $this->data,
            'message'   => $this->message,
        );
    }

    public function toJSON():string
    {
        return json_encode($this->toArray());
    }

    /**
     * @throws MissingEntityException
     */
    public function toEntity($entity = null)
    {
        if (empty($entity) && empty($this->entityClassToTransform)) {
            throw new MissingEntityException();
        }
        $class = $entity??$this->entityClassToTransform;
        return new $class($this->getDataFromJson());
    }

    /**
     * @return bool
     */
    public function isSuccess():bool
    {
        return $this->isSuccess;
    }

    /**
     * @return array | Exception
     */
    public function getData()
    {
        return $this->data;
    }

    public function getDataFromJson():array
    {
        if ($this->data instanceof Exception) {
            return $this->data->getTrace();
        }
        return json_decode($this->data, true);
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}
