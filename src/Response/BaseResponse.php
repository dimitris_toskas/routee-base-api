<?php
declare(strict_types=1);

namespace RouteeBaseApi\Response;

use GuzzleHttp\Exception\ClientException;
use Psr\Http\Message\ResponseInterface;

class BaseResponse implements iResponse
{
    /**
     * @var ResponseInterface $guzzleResponse
     */
    protected $guzzleResponse;
    public function __construct($response)
    {
        if ($response instanceof ClientException) {
            $this->guzzleResponse = $response->getResponse();
        } else {
            $this->guzzleResponse = $response;
        }
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->guzzleResponse->getStatusCode();
    }

    public function decodeContent():array
    {
        $content = $this->getContents();
        if (!empty($content)) {
            return json_decode($content, true);
        }
        return [];
    }

    /**
     * @return string
     */
    public function getContents(): string
    {
        return $this->guzzleResponse->getBody()->getContents();
    }

    /**
     * @return ResponseInterface
     */
    public function getGuzzleResponse(): ResponseInterface
    {
        return $this->guzzleResponse;
    }
}
