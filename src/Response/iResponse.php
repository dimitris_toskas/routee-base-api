<?php
declare(strict_types=1);

namespace RouteeBaseApi\Response;

interface iResponse
{
    public function getStatusCode();
    public function decodeContent();
    public function getContents();
    public function getGuzzleResponse();
}
