<?php
declare(strict_types=1);

namespace RouteeBaseApi\Exception;

use RouteeBaseApi\Enum\BaseApiErrorCodeEnum;
use Throwable;

class MissingEntityException extends BaseApiException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, BaseApiErrorCodeEnum::MISSING_ENTITY, $previous);
    }
}
