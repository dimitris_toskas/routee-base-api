<?php
declare(strict_types=1);

namespace RouteeBaseApi\Exception;

use RouteeBaseApi\Enum\BaseApiErrorCodeEnum;
use Throwable;

abstract class BaseApiException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if (empty($message)) {
            $message = BaseApiErrorCodeEnum::getMessage($code);
        }
        parent::__construct($message, $code, $previous);
    }
}
