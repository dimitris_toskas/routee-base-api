<?php
declare(strict_types=1);

namespace RouteeBaseApi\Exception;

use RouteeBaseApi\Enum\BaseApiErrorCodeEnum;

class MissingRequiredFieldException extends BaseApiException
{
    protected $code = BaseApiErrorCodeEnum::MISSING_REQUEST_FIELD;
    public function __construct($fieldname = "")
    {
        $message = BaseApiErrorCodeEnum::getMessage(
            BaseApiErrorCodeEnum::MISSING_REQUEST_FIELD,
            ['fieldname' => $fieldname]
        );
        parent::__construct($message, BaseApiErrorCodeEnum::MISSING_REQUEST_FIELD);
    }
}
