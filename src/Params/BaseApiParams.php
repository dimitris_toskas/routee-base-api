<?php
declare(strict_types=1);

namespace RouteeBaseApi\Params;

class BaseApiParams
{
    public function __construct(array $data = [])
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    public function toArray():array
    {
        return json_decode(json_encode($this), true);
    }
}
