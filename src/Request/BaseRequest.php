<?php
declare(strict_types=1);

namespace RouteeBaseApi\Request;

use GuzzleHttp\Client;
use http\Header;
use \Psr\Http\Message\ResponseInterface;
use RouteeBaseApi\Enum\ContentTypeEnum;
use RouteeBaseApi\Enum\GuzzleEnum;
use RouteeBaseApi\Enum\HeadersEnum;
use RouteeBaseApi\Enum\HttpMethodEnum;
use RouteeBaseApi\Exception\MissingActionException;
use RouteeBaseApi\Exception\MissingRequiredFieldException;
use RouteeBaseApi\Response\ApiResponse;
use RouteeBaseApi\Response\BaseResponse;
use RouteeBaseApi\Response\iResponse;
use RouteeBaseApi\Utils\UrlHandler;

abstract class BaseRequest implements iBaseRequest
{
    /**
     * @var Client|null
     */
    protected $client = null;
    protected $baseUrl = '';
    protected $action = '';
    protected $method = '';
    protected $params = [];
    protected $headers = [];
    protected $responseType = '';
    protected $paramsType = '';
    protected $auth = [];
    protected $requiredParams = [];

    public function __construct($url = null, $method = HttpMethodEnum::GET, $params = [], $headers = [])
    {
        $this->setBaseUrl($url);
        $this->setMethod($method);
        $this->setParams($params);
        if (!isset($headers[HeadersEnum::ACCEPT])) {
            $headers[HeadersEnum::ACCEPT] = ContentTypeEnum::APPLICATION_JSON;
        }
        if (!isset($headers[HeadersEnum::CONTENT_TYPE])) {
            $headers[HeadersEnum::CONTENT_TYPE] = ContentTypeEnum::APPLICATION_JSON;
        }
        $this->setHeaders($headers);
        $this->client = new Client();
    }

    /******************
     * Public Methods *
     ******************/

    public function getUrl(): string
    {
        return UrlHandler::mergeUrl($this->getBaseUrl(), $this->getAction());
    }

    /**
     * @throws MissingRequiredFieldException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws MissingActionException
     * @throws \Exception
     */
    public function execute(): iResponse
    {
        $this->validateRequest();
        $returnType = $this->getResponseType();
        try {
            $guzzleRequest = $this->client->request($this->getMethod(), $this->getUrl(), $this->getOptions());
        } catch (\Exception $ex) {
            if ($returnType === ApiResponse::class
                || is_subclass_of($returnType, ApiResponse::class)
            ) {
                $guzzleRequest = $ex;
            } else {
                throw $ex;
            }
        }
        if (!empty($returnType)) {
            return new $returnType($guzzleRequest);
        }
        return new BaseResponse($guzzleRequest);
    }

    /*********************
     * Protected Methods *
     *********************/

    protected function getResponseType():string
    {
        return $this->responseType;
    }
    protected function setResponseType(string $responseType)
    {
        $this->responseType = $responseType;
    }
    protected function setParams(array $params)
    {
        $this->params = $params;
    }
    protected function getParams(): array
    {
        return $this->params;
    }
    protected function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    protected function getOptions():array
    {
        return array_merge($this->buildParams(), $this->buildHeader(), $this->getAuth());
    }

    /**
     * @throws MissingRequiredFieldException
     * @throws MissingActionException
     */
    protected function validateRequest()
    {
        $this->validateAction();
        $this->validateRequiredParams();
    }

    /**
     * @throws MissingActionException
     */
    protected function validateAction()
    {
        if (empty($this->getAction())) {
            throw new MissingActionException();
        }
    }

    /**
     * @throws MissingRequiredFieldException
     */
    protected function validateRequiredParams():void
    {
        $required = $this->getRequiredParams();
        $params = $this->getParams();
        if (!empty($required)) {
            foreach ($required as $reqParam) {
                if (!isset($params[$reqParam])) {
                    throw new MissingRequiredFieldException($reqParam);
                }
            }
        }
    }

    protected function buildParams($type = null): array
    {
        $requestParams = $this->getParams();
        $result = [];
        if (!empty($requestParams)) {
            $key = $this->getParamsType();
            if (empty($key)) {
                switch ($this->getMethod()) {
                    case HttpMethodEnum::GET:
                        $key = GuzzleEnum::QUERY;
                        break;
                    default:
                        $key = GuzzleEnum::FORM_PARAMS;
                }
            }
            $result[$key] = $requestParams;
        }

        return $result;
    }

    protected function buildHeader(): array
    {
        if (empty($this->headers)) {
            return [];
        }
        return [
            'headers' => $this->headers
        ];
    }

    /**
     * @param string $baseUrl
     */
    protected function setBaseUrl(string $baseUrl)
    {
        $this->baseUrl = $baseUrl;
    }

    protected function getBaseUrl():string
    {
        return $this->baseUrl;
    }
    protected function getParamsType():string
    {
        return $this->paramsType;
    }

    /**
     * @param string $type
     */
    protected function setParamsType(string $type)
    {
        $this->paramsType = $type;
    }

    /**
     * @return string
     */
    protected function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    protected function setAction(string $action):void
    {
        $this->action = $action;
    }

    /**
     * @param string $method
     */
    protected function setMethod(string $method):void
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    protected function getMethod(): ?string
    {
        return $this->method;
    }

    protected function getClient():Client
    {
        return $this->client;
    }

    protected function setRequiredParams(array $params = []):void
    {
        $this->requiredParams = $params;
    }

    protected function getRequiredParams():array
    {
        return $this->requiredParams;
    }

    /**
     * @param string $username
     * @param string $password
     */
    protected function setAuth(string $username, string $password):void
    {
        $this->auth = [
            'auth' => [$username, $password]
        ];
    }

    protected function getAuth():array
    {
        return $this->auth;
    }
}
