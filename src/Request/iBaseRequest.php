<?php
declare(strict_types=1);

namespace RouteeBaseApi\Request;

use RouteeBaseApi\Response\iResponse;

interface iBaseRequest
{
    public function execute():iResponse;
    public function getUrl():string;
}
