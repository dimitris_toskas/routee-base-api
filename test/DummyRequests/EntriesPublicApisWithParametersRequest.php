<?php
namespace RouteeBaseApi\Tests\DummyRequests;

use RouteeBaseApi\Enum\HttpMethodEnum;

/**
 * This example uses the below public API from DigitalOcean
 * https://api.publicapis.org/
 * Class EntriesPublicApisRequest
 */
class EntriesPublicApisWithParametersRequest extends EntriesPublicApisBaseRequest
{
    protected $requiredParams = array(
        'title'
    );

    public function __construct($baseUrl = 'https://api.publicapis.org', $method = HttpMethodEnum::GET)
    {
        parent::__construct($baseUrl, $method);
        $this->setAction('/entries');
    }

    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->params['title'];
    }

    /**
     * @param null $title
     */
    public function setTitle($title)
    {
        $this->params['title'] = $title;
    }
}
