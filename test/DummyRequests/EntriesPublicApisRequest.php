<?php
namespace RouteeBaseApi\Tests\DummyRequests;

use RouteeBaseApi\Enum\HttpMethodEnum;

/**
 * This example uses the below public API from DigitalOcean
 * https://api.publicapis.org/
 * Class EntriesPublicApisRequest
 */
class EntriesPublicApisRequest extends EntriesPublicApisBaseRequest
{
    public function __construct($baseUrl = 'https://api.publicapis.org', $method = HttpMethodEnum::GET)
    {
        parent::__construct($baseUrl, $method);
        $this->setAction('/entries');
    }


}
