<?php
namespace RouteeBaseApi\Tests\DummyRequests;

use RouteeBaseApi\Enum\HttpMethodEnum;
use RouteeBaseApi\Request\BaseRequest;

abstract class EntriesPublicApisBaseRequest extends BaseRequest
{
    public function __construct($baseUrl = 'https://api.publicapis.org', $method = HttpMethodEnum::GET)
    {
        parent::__construct($baseUrl, $method);
    }

    /**
     * Helper functions to expose the protected functions for unit testing
     */

    public function setActionPublic($action)
    {
        $this->setAction($action);
    }
    public function getActionPublic()
    {
        return $this->getAction();
    }
    public function setMethodPublic($method)
    {
        $this->setMethod($method);
    }
    public function getMethodPublic()
    {
        return $this->getMethod();
    }
    public function setBaseUrlPublic($url)
    {
        $this->setBaseUrl($url);
    }
    public function getBaseUrlPublic()
    {
        return $this->getBaseUrl();
    }
    public function setRequiredParamsPublic($fields = [])
    {
        $this->setRequiredParams($fields);
    }
    public function getRequiredParamsPublic()
    {
        return $this->getRequiredParams();
    }
    public function setResponseTypePublic($responseType = "")
    {
        $this->setResponseType($responseType);
    }
    public function getResponseTypePublic()
    {

        return $this->getResponseType();
    }
}