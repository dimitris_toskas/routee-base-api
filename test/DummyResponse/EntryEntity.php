<?php
declare(strict_types=1);

namespace RouteeBaseApi\Tests\DummyResponse;

class EntryEntity
{
    public function __construct($data = [])
    {
        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
        }
    }
}
