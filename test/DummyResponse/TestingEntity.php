<?php
declare(strict_types=1);

namespace RouteeBaseApi\Tests\DummyResponse;

class TestingEntity
{
    protected $count = 0;
    protected $entries = [];

    public function __construct($data = [])
    {
        if (isset($data['count'])) {
            $this->count = $data['count'];
        }
        if (!empty($data['entries'])) {
            foreach ($data['entries'] as $entry) {
                array_push($this->entries, new EntryEntity($entry));
            }
        }
    }
}
