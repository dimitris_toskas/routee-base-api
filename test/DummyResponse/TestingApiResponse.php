<?php
declare(strict_types=1);

namespace RouteeBaseApi\Tests\DummyResponse;

use RouteeBaseApi\Response\ApiResponse;

class TestingApiResponse extends ApiResponse
{
    public function __construct($response)
    {
        parent::__construct($response);
        $this->entityClassToTransform = TestingEntity::class;
    }
}