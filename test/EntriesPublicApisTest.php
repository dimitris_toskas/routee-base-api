<?php

namespace RouteeBaseApi\Tests;

use Psr\Http\Message\ResponseInterface;
use RouteeBaseApi\Enum\BaseApiErrorCodeEnum;
use RouteeBaseApi\Enum\HttpMethodEnum;
use RouteeBaseApi\Exception\MissingActionException;
use RouteeBaseApi\Exception\MissingRequiredFieldException;
use RouteeBaseApi\Response\ApiResponse;
use RouteeBaseApi\Tests\DummyRequests\EntriesPublicApisRequest;
use RouteeBaseApi\Tests\DummyRequests\EntriesPublicApisWithParametersRequest;
use RouteeBaseApi\Tests\DummyResponse\TestingApiResponse;
use RouteeBaseApi\Tests\DummyResponse\TestingEntity;
use RouteeBaseApi\Tests\DummyResponse\TestingResponse;

class EntriesPublicApisTest extends BaseRequestTest
{
    /**
     * @var EntriesPublicApisRequest $request
     */
    protected $request = null;
    /**
     * @var EntriesPublicApisWithParametersRequest $requestWithParams
     */
    protected $requestWithParams = null;
    protected function setUp():void
    {
        parent::setUp();
        $this->request = new EntriesPublicApisRequest();
        $this->requestWithParams = new EntriesPublicApisWithParametersRequest();
    }

    public function testGetRequestWithParams()
    {
        $title = 'cat';
        $this->requestWithParams->setTitle($title);
        $response = $this->requestWithParams->execute();
        $this->assertEquals('200', $response->getStatusCode());
        $body = json_decode($response->getContents(), true);
        $entries = $body['entries'];
        $isValid = true;
        foreach ($entries as $entry) {
            if (stripos(strtolower($entry['API']), $title) === false) {
                $isValid = false;
                break;
            }
        }
        $this->assertTrue($isValid);
    }

    public function testMissingRequiredField()
    {
        $expectedMessage = 'Required field "title" is missing';
        $this->requestWithParams->setTitle(null);
        try {
            $response = $this->requestWithParams->execute();
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            $this->assertEquals($expectedMessage, $message);
            $this->assertInstanceOf(MissingRequiredFieldException::class, $ex);
        }
    }

    public function testGetRequest()
    {
        $response = $this->request->execute();
        //check the response code
        $this->assertEquals('200', $response->getStatusCode());
        $body = json_decode($response->getContents(), true);
        $this->assertEquals($body['count'], count($body['entries']));
        $this->assertInstanceOf(ResponseInterface::class, $response->getGuzzleResponse());
    }

    public function testDecodeContent()
    {
        $response = $this->request->execute();
        $actual = $response->decodeContent();
        $response = $this->request->execute(); //execute again to load the stream buffer to decode it manually
        $expected = json_decode($response->getContents(), true);
        $this->assertEquals($expected, $actual);
    }

    public function testGet404Request()
    {
        $this->request->setActionPublic('/wrongUrl');
        $this->expectExceptionCode(404);
        $response = $this->request->execute();
    }

    public function testSetgetMethodPublic()
    {
        $this->request->setMethodPublic(HttpMethodEnum::PATCH);
        $this->assertEquals(HttpMethodEnum::PATCH, $this->request->getMethodPublic());
    }

    public function testSetGetActionPublic()
    {
        $this->request->setActionPublic('/api-url');
        $this->assertEquals('/api-url', $this->request->getActionPublic());
    }

    public function testMissingAction()
    {
        $this->request->setActionPublic("");
        try {
            $response = $this->request->execute();
        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            $this->assertEquals(
                BaseApiErrorCodeEnum::getMessage(
                    BaseApiErrorCodeEnum::MISSING_ACTION
                ),
                $message
            );
            $this->assertInstanceOf(MissingActionException::class, $ex);
        }
    }

    public function testSetgetBaseUrlPublic()
    {
        $this->request->setBaseUrlPublic('http://base-api-url');
        $this->assertEquals('http://base-api-url', $this->request->getBaseUrlPublic());
    }

    public function testSetGetRequiredParams()
    {
        $params = array(
            'field1',
            'field2',
            'field3',
        );
        $this->request->setRequiredParamsPublic($params);
        $this->assertEquals($params, $this->request->getRequiredParamsPublic());
    }

    public function testResponseTypeChange()
    {
        $this->request->setResponseTypePublic(TestingResponse::class);
        $response = $this->request->execute();
        $this->assertInstanceOf(TestingResponse::class, $response);
    }

    public function testApiResponse()
    {
        $this->request->setResponseTypePublic(ApiResponse::class);
        /**
         * @var ApiResponse $response
         */
        $response = $this->request->execute();
        $this->assertTrue($response->isSuccess());
        $responseArray = $response->toArray();
        $this->assertTrue(isset($responseArray['isSuccess']));
        $this->assertTrue(isset($responseArray['data']));
        $this->assertTrue(isset($responseArray['message']));
        $response->setMessage("Overwrite of message");
        $this->assertEquals("Overwrite of message", $response->getMessage());
        $json = $response->toJSON();
        $jsonDecode = json_decode($json);
        $this->assertEmpty(json_last_error());
        $this->request->setActionPublic('whatever');
        $response = $this->request->execute();
        $this->assertFalse($response->isSuccess());
    }

    public function testApiEntity()
    {
        $title = 'cat';
        $this->requestWithParams->setTitle($title);
        $this->requestWithParams->setResponseTypePublic(TestingApiResponse::class);
        /**
         * @var TestingResponse $response
         */
        $response = $this->requestWithParams->execute();
        $entity = $response->toEntity();
        $this->assertInstanceOf(TestingEntity::class, $entity);
    }
}
